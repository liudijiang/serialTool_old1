# serialTool
### 介绍
使用PyQt5开发串口助手，显示框和输入框可以随窗口拉伸变化。
![serialTool界面](https://gitee.com/liudijiang/serialTool/blob/master/serialTool.png "serialTool界面")
### 开发平台
Pycharm + PyQt5 + Python3.7
### 使用说明
需要配置Pycharm开发环境
-PyQt5
-PyQt5-tools
-pyserial
### bug
*仅支持GB2312编码，待增加utf-8编码
*接收过快数据时，会导致decode失败，从而有乱码出现
*窗体图标的路径为绝对路径，需要修复
